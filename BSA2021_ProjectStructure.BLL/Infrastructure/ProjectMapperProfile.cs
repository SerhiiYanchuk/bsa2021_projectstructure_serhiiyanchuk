﻿using AutoMapper;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.BLL.Infrastructure
{
    public class ProjectMapperProfile : Profile
    {
        public ProjectMapperProfile()
        {
            CreateMap<Project, ProjectDTO>().ReverseMap();
            CreateMap<Task, TaskDTO>().ReverseMap();
            CreateMap<Team, TeamDTO>().ReverseMap();
            CreateMap<User, UserDTO>().ReverseMap();

            CreateMap<NewProjectDTO, Project>();
            CreateMap<NewTaskDTO, Task>();
            CreateMap<NewTeamDTO, Team>();
            CreateMap<NewUserDTO, User>();
        }
    }
}