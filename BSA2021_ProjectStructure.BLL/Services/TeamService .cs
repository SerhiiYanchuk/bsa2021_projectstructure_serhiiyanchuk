﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<TeamDTO> FindAll()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_unitOfWork.Teams.FindAll());
        }
        public TeamDTO FindById(int teamId)
        {
            return _mapper.Map<TeamDTO>(_unitOfWork.Teams.FindById(teamId));
        }
        public TeamDTO Insert(NewTeamDTO teamDTO)
        {         
            var createdTeam = _unitOfWork.Teams.Insert(_mapper.Map<Team>(teamDTO));
            return _mapper.Map<TeamDTO>(createdTeam);
        }

        public void Update(TeamDTO teamDTO)
        {
            _unitOfWork.Teams.Update(_mapper.Map<Team>(teamDTO));
        }

        public void Delete(int teamId)
        {
            _unitOfWork.Teams.Delete(teamId);
        }

        public void Delete(TeamDTO teamDTO)
        {
            _unitOfWork.Teams.Delete(_mapper.Map<Team>(teamDTO));
        }
        public bool CheckAvailability(int teamId)
        {
            return _unitOfWork.Teams.CheckAvailability(teamId);
        }
       
    }
}
