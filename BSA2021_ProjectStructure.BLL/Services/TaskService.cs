﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<TaskDTO> FindAll()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_unitOfWork.Tasks.FindAll());
        }
        public TaskDTO FindById(int taskId)
        {
            return _mapper.Map<TaskDTO>(_unitOfWork.Tasks.FindById(taskId));
        }
        public TaskDTO Insert(NewTaskDTO taskDTO)
        {          
            var createdTask = _unitOfWork.Tasks.Insert(_mapper.Map<Task>(taskDTO));
            return _mapper.Map<TaskDTO>(createdTask);
        }

        public void Update(TaskDTO taskDTO)
        {
            _unitOfWork.Tasks.Update(_mapper.Map<Task>(taskDTO));
        }

        public void Delete(int taskId)
        {
            _unitOfWork.Tasks.Delete(taskId);
        }

        public void Delete(TaskDTO taskDTO)
        {
            _unitOfWork.Tasks.Delete(_mapper.Map<Task>(taskDTO));
        }
        public bool CheckAvailability(int taskId)
        {
            return _unitOfWork.Tasks.CheckAvailability(taskId);
        }
       
    }
}
