﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<UserDTO> FindAll()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_unitOfWork.Users.FindAll());
        }
        public UserDTO FindById(int userId)
        {
            return _mapper.Map<UserDTO>(_unitOfWork.Users.FindById(userId));
        }
        public UserDTO Insert(NewUserDTO userDTO)
        {
            var createdUser = _unitOfWork.Users.Insert(_mapper.Map<User>(userDTO));
            return _mapper.Map<UserDTO>(createdUser);
        }

        public void Update(UserDTO userDTO)
        {
            _unitOfWork.Users.Update(_mapper.Map<User>(userDTO));
        }

        public void Delete(int userId)
        {
            _unitOfWork.Users.Delete(userId);
        }

        public void Delete(UserDTO userDTO)
        {
            _unitOfWork.Users.Delete(_mapper.Map<User>(userDTO));
        }
        public bool CheckAvailability(int userId)
        {
            return _unitOfWork.Users.CheckAvailability(userId);
        }
       
    }
}
