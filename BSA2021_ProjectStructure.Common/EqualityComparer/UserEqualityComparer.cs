﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace BSA2021_ProjectStructure.Common.EqualityComparer
{
    public class UserEqualityComparer : IEqualityComparer<UserDTO>
    {
        public bool Equals(UserDTO x, UserDTO y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] UserDTO obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
