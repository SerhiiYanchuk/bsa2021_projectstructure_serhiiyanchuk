﻿using System;


namespace BSA2021_ProjectStructure.Common.DTO
{
    public class NewTaskDTO
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStateDTO State { get; set; } = TaskStateDTO.Unfinished;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
