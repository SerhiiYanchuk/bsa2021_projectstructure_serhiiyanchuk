﻿using System.Collections.Generic;

namespace BSA2021_ProjectStructure.Common.DTO.QueryResultDTO
{
    public class PerformerWithTasksDTO: UserDTO
    {
        public List<TaskDTO> Tasks { get; set; } = new List<TaskDTO>();
    }
}
