﻿using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
        // you can add specific functionality for Project
    }
}
