﻿using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IEnumerable<TEntity> FindAll();
        TEntity FindById(int id);
        IEnumerable<TEntity> Find(Func<TEntity, bool> predicate);
        TEntity Insert(TEntity item);
        void Update(TEntity item);
        void Delete(int id);
        void Delete(TEntity item);
        bool CheckAvailability(int id);
    }
}
