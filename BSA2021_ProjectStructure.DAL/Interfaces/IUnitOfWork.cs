﻿using System;
using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProjectRepository Projects { get; }
        ITaskRepository Tasks { get; }
        ITeamRepository Teams { get; }
        IUserRepository Users { get; }
        bool SaveChanges();
    }
}
