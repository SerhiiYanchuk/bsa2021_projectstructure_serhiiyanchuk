﻿using System.Collections.Generic;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class TaskRepository : Repository<Task>, ITaskRepository
    {
        public TaskRepository(List<Task> data) : base(data)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
    }
}
