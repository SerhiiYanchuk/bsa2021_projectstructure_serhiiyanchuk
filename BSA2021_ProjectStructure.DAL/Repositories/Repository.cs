﻿using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity: Entity
    {
        protected readonly List<TEntity> _data;
        public Repository(List<TEntity> data)
        {
            _data = data;
        }
        public virtual IEnumerable<TEntity> FindAll()
        {
            return _data;
        }
        public virtual TEntity FindById(int id)
        {
            return _data.FirstOrDefault(p => p.Id == id);
        }
        public virtual IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
            return _data.Where(predicate);
        }
        public virtual TEntity Insert(TEntity item)
        {
            int freeId = _data.Max(p => p.Id) + 1;
            item.Id = freeId;
            _data.Add(item);
            return item;
        }
        public virtual void Update(TEntity item)
        {
            int index = _data.FindIndex(p => p.Id == item.Id);
            if (index == -1)
                return;

            _data.RemoveAt(index);
            _data.Add(item);
        }
        public virtual void Delete(int id)
        {
            int index = _data.FindIndex(p => p.Id == id);
            if (index == -1)
                return;

            _data.RemoveAt(index);
        }
        public virtual void Delete(TEntity item)
        {
            Delete(item.Id);
        }
        public virtual bool CheckAvailability(int id)
        {
            return _data.Exists(p => p.Id == id);
        }
    }
}
