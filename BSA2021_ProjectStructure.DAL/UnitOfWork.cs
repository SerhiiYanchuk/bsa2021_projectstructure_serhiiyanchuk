﻿using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using BSA2021_ProjectStructure.DAL.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BSA2021_ProjectStructure.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IProjectRepository _projectRepository;
        private readonly ITaskRepository _taskRoomRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;
        public UnitOfWork()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = GetData(client, "Projects");
                _projectRepository = new ProjectRepository(JsonConvert.DeserializeObject<List<Project>>(response.Content.ReadAsStringAsync().Result));

                response = GetData(client, "Tasks");
                _taskRoomRepository = new TaskRepository(JsonConvert.DeserializeObject<List<Task>>(response.Content.ReadAsStringAsync().Result));

                response = GetData(client, "Teams");
                _teamRepository = new TeamRepository(JsonConvert.DeserializeObject<List<Team>>(response.Content.ReadAsStringAsync().Result));

                response = GetData(client, "Users");
                _userRepository = new UserRepository(JsonConvert.DeserializeObject<List<User>>(response.Content.ReadAsStringAsync().Result));
            }
        }
        public IProjectRepository Projects
        {
            get
            {
                return _projectRepository;
            }
        }
        public ITaskRepository Tasks
        {
            get
            {
                return _taskRoomRepository;
            }
        }
        public ITeamRepository Teams
        {
            get
            {
                return _teamRepository;
            }
        }
        public IUserRepository Users
        {
            get
            {
                return _userRepository;
            }
        }
      
        public bool SaveChanges()
        {
            return true;
        }
        public void Dispose() { }
        private HttpResponseMessage GetData(HttpClient httpClient, string route)
        {
            var response = httpClient.GetAsync($"https://bsa21.azurewebsites.net/api/{route}").Result;
            if (!response.IsSuccessStatusCode)
                throw new Exception($"Status: {response.StatusCode}, {response.Content}");
            return response;
        }
    }
}
