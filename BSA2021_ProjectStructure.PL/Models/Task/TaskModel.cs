﻿using System;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public UserModel Performer { get; set; } 
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStateModel State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
