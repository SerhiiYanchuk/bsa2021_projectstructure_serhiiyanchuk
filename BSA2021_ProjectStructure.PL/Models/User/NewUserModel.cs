﻿using System;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class NewUserModel
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; } = DateTime.Now;
        public DateTime BirthDay { get; set; }
    }
}


