﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class TeamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<UserModel> Members { get; set; } = new List<UserModel>();
    }
}
