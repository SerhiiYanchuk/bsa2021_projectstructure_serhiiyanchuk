﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.PL.Interfaces
{
    public interface ICrudService<TEntity, TNewEntity>
    {
        IModelFactory<TEntity, TNewEntity> ModelFactory { get; }
        IEnumerable<TEntity> FindAll();
        TEntity FindById(int id);
        TEntity Insert(TNewEntity item);
        void Update(TEntity item);
        void Delete(int id);
    }
}
