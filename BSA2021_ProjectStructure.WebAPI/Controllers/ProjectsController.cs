﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly ILinqSelectionService _linqSelectionService;

        public ProjectsController(IProjectService projectService, ILinqSelectionService linqSelectionService)
        {
            _projectService = projectService;
            _linqSelectionService = linqSelectionService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetProjects()
        {
            return Ok(_projectService.FindAll());
        }

        [HttpGet("detail")]
        public ActionResult<IEnumerable<ProjectDetailDTO>> GetProjectsDetail()
        {
            return Ok(_linqSelectionService.GetProjectsDetail());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetProject(int id)
        {
            var project = _projectService.FindById(id);
            if (project is null)
                return NotFound("ID doesn't exist");
            return Ok(project);
        }

        [HttpPost]
        public ActionResult<ProjectDTO> PostProject([FromBody] NewProjectDTO projectDTO)
        {
            var createdProject = _projectService.Insert(projectDTO);
            return Created("/api/projects/" + createdProject.Id, createdProject);
        }

        [HttpPut]
        public IActionResult PutProject([FromBody] ProjectDTO projectDTO)
        {
            if (!_projectService.CheckAvailability(projectDTO.Id))
                return NotFound("ID doesn't exist");

            _projectService.Update(projectDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            if (!_projectService.CheckAvailability(id))
                return NotFound("ID doesn't exist");

            _projectService.Delete(id);
            return NoContent();
        }
    }
}
