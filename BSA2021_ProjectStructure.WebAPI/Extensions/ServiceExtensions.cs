﻿using BSA2021_ProjectStructure.BLL.Infrastructure;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.BLL.Services;
using BSA2021_ProjectStructure.DAL;
using BSA2021_ProjectStructure.DAL.Interfaces;
using Microsoft.Extensions.DependencyInjection;


namespace BSA2021_ProjectStructure.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ILinqSelectionService, LinqSelectionService>();
        }
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg => cfg.AddProfile(new ProjectMapperProfile()));
        }
    }
}
